<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title></title>
	<meta name="" content="">
	<link rel="stylesheet" type="text/css" media="screen" href="jquery.dataTables.css"/>
	<script type="text/javascript" src="http://dev.exportportal.com/public/js/jquery.min.js"></script>
	<script type="text/javascript" src="jquery.dataTables.js"></script>
	<script type="text/javascript" src="jquery.dtFilters.js"></script>
</head>
<body>
<script type="text/javascript">
$(document).ready(function(){
	var	usersFilters;
	
	var dtUsersList = $('#dt-users').dataTable( {
		"sDom": '<"top"lp>rt<"bottom"ip><"clear">',
		"bProcessing": true,
		"bServerSide": true,
		"eventSearch": "change",
		"sAjaxSource": "/ajax.php",
		"sServerMethod": "POST",
		"iDisplayLength": 10,
		"aLengthMenu": [
		    [10, 25, 50, 100, 0],
		    [10, 25, 50, 100, 'All']
		],
		"aoColumnDefs": [
			{ "sClass": "w-50 tac vam", "aTargets": ["dt_idu"], "mData": "dt_idu", "bSortable": false }, //id
			{  "aTargets": ["dt_new"], "mData": "dt_new" },
			{  "aTargets": ["dt_post"], "mData": "dt_post" }, // names
						
		],
		"aaSorting" : [[1,'asc']],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			//begin
			if(!usersFilters){
				usersFilters = $('.plug').dtFilters({
					'container': '.container',
					'ulClass': 'dsadsa',
					'debug' : true,
					callBack: function(){
						dtUsersList.fnDraw();
					},
					onSet: function(callerObj, filterObj){ 
						//alert('my Set' + " | " +  callerObj.tagName + " | " +  filterObj.name); //for testing
					},
					onDelete: function(filterObj){
						//alert('Filter '+ filterObj.label + ' was deleted');
					}
				});
			}
			
			aoData = aoData.concat(usersFilters.getDTFilter());
			//end

			$.ajax( {
			    "dataType": 'json',
			    "type": "POST",
			    "url": sSource,
			    "data": aoData,
			    "success": function (data, textStatus, jqXHR) {
					if(data.mess_type == 'error')
						systemMessages(data.message, 'message-' + data.mess_type);

					fnCallback(data, textStatus, jqXHR);
			    },

			} );
		},
		"sPaginationType": "full_numbers",
		"fnDrawCallback": function( oSettings ) {

		}
	});	

	$("a.get_but").live('click', function(){
		var html_b = "<input type='checkbox' class='plug' name='dataa' value='1' data-title='Dataa' data-value-text='Yes' />                         <input type='checkbox' class='plug' name='dataa' value='2' data-value-text='No' />                         <input type='checkbox' class='plug' name='dataa' value='3' data-value-text='Ok' />";
        $('.filter-admin-panel').first().prepend(html_b);
        usersFilters.reInit();
	})

})
</script>
<style>
.dt-filter-apply-active{
	background-color: red; 
}
</style>
<div class="grid_12">
	<div class="filter-admin-panel">
	    <button><a class='get_but'>GET</a></button>
		| <span>Title Select:</span>
		<select class="plug" id="a1" name="status" data-title="Status">
			<option value="">All</option>
			<option value="1">new</option>
			<option value="2">active</option>
			<option value="3">banned</option>
		</select>
		<br/>
		| <span>Date from:</span><input type="text" class="plug" name="data" id="a2" value="" data-title="Date from" />
		<br/>
		| <span>Search:</span><input type="text" class="plug" name="search" value="" data-title="Search" />
		<br/>
		| <span>Moderated:</span>
		<input type="radio" class="plug" name="moderate" value="1" data-title="Moderated" data-value-text="Yes"/><label>yes</label>
		<input type="radio" class="plug" name="moderate" value="0" data-value-text="No"/><label>no</label>
		<input type="radio" class="plug" name="moderate" value="" data-value-text="All"/><label>all</label>
		<br>
		| <span>Online/Offline:</span>
		<select class="plug" id="a1ds" name="on_off" data-title="Online/Offline">
			<option value="" data-value-text="all">All</option>
			<option value="1" data-value-text="onlineeee">Online</option>
			<option value="2" data-value-text="offlinee">Offline</option>
		</select>
        <br>
		| <span>Multiselect:</span>
		<select class="plug" id="a1ds" multiple name="multi" data-title="Multiselect">
			<option value="0">a0</option>
			<option value="1">a1</option>
			<option value="2">a2</option>
		</select>		
		<button><a class='plug' data-name='multi' data-value="0,2" >1,3</a></button>
		<br/>
		<!--| <span>Show:</span>
		<input type="checkbox" class="plug" name="dataa" value="1" data-title="Show" data-value-text="Yes" />
		<input type="checkbox" class="plug" name="dataa" value="2" data-value-text="No" />
		<input type="checkbox" class="plug" name="dataa" value="3" data-value-text="OK" />
		<button><a class='plug' data-name='dataa' data-value="1,3" >1,3</a></button>
		<br/>-->
		| <span>Groups:</span>
		<button><a class='plug' data-name='groups' data-title='Groups' data-value='4' data-value-text='All'>All</a></button>
		<button><a class='plug' data-name='groups' data-title='Groups' data-value='1' data-value-text='Sellers'>Sellers</a></button>
		<button><a class='plug' data-name='groups' data-title='Groups' data-value='2' data-value-text='Buyers'>Buyers</a></button>
		<button><a class='plug' data-name='groups' data-title='Groups' data-value='3' data-value-text='Verified'>Verified</a></button>
		<br/>
		<button class="dt-filter-apply-buttons">Apply</button><button class="dt-filter-reset-buttons">Reset</button>
	</div>
	<div class="container"></div>
	<div class="container"></div>
    <div class=" w-100pr pull-left">
        <table class="data w_zebra w-100pr " id="dt-users">
            <thead>
                <tr>
                    <th class="first dt_idu"><input type='checkbox' id='check_all' class="pull-left"/>#</th>
                    <th class="tac dt_new">New<input type="text" class="plug" name="keywords_new" value="" data-title="Search by new" /></th>
                    <th class="dt_post">Post<input type="text" class="plug" name="keywords_post" value="" data-title="Search by Post" /></th>
                </tr>
            </thead>

            <tbody class="tabMessage" id="pageall">
            </tbody>
        </table>
    </div>
	
</div>
</body>
</html>